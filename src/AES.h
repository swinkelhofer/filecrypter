#include <stdlib.h>
#include <stdio.h>
#include "CryptString.h"

class AES
{

public:
	enum BlockMode {CBC=0,ECB=1};
	AES();
	//AES(int keylength, BlockMode block);
	//AES(CryptString key, int keylength = 256, BlockMode block = AES::CBC);
	//static EncryptFile(char *fileName, char *newFileName, CryptString key, int keylength=256, BlockMode = AESClass::CBC);
	
	//SHA256(char *pw, unsigned int out[8]);
	
	void EncryptFile(const char* infile, const char* outfile, CryptString key, int keylength = 256, BlockMode block = AES::CBC);
	void EncryptFile(const char* infile, const char* outfile, unsigned int *key, int keylength = 256, BlockMode block = AES::CBC);
	void DecryptFile(const char* infile, const char* outfile, CryptString key, int keylength = 256, BlockMode block = AES::CBC);
	void EncryptBlock256(const unsigned char in[16], unsigned char out[16], const unsigned char key[32]);
	void DecryptBlock256(const unsigned char in[16], unsigned char out[16], const unsigned char key[32]);
private:
	unsigned char State[16];
	unsigned char KeyBlock[240];
	int r;
	int Nr;	//Rounds
	int Nk;	//Keylength
	int Rx4;
	unsigned int *cur, *key;
	
	
	void keyExpansion();
	
	void selectParameters();
	
	void addRoundKey();
	void mixColumns();
	void shiftRows();
	void subBytes();
	void printKey();
	void Combined();
	void InvCombined();
	void FirstRound();
	void LastRound();
	void InvLastRound();
	
	void FastBlockEnc256();
	void FastBlockDec256();

};
