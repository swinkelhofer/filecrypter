#include "AES.h"
#include "CryptString.h"
#include <sys/time.h>
#include <sys/stat.h>
#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
using namespace std;

struct _args {
    char *key;
    int numfiles;
    char **files;
    int mode; // 0 = enc 1 = dec
} args;

void help() {
    printf("Usage: filecrypter [-d | --decrypt] [-e | --encrypt] [-h | --help] <-p | --password> <file1>...<fileN>\n");
}

void parse_args(int argc, char *argv[]) {
    int enc_dec_count = 0;
    args.numfiles = 0;
    char *key = NULL;
    args.files = (char**)malloc(sizeof(char*)*argc);
    for(int i = 1; i < argc; ++i) {
        if (strcmp(argv[i], "-d") == 0 || strcmp(argv[i], "--decrypt") == 0) {
            args.mode = 1;
            enc_dec_count++;
        }
        else if (strcmp(argv[i], "-e") == 0 || strcmp(argv[i], "--encrypt") == 0) {
            args.mode = 0;
            enc_dec_count++;
        }
        else if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0) {
            help();
            exit(0);
        }
        else if (strcmp(argv[i], "-p") == 0 || strcmp(argv[i], "--password") == 0) {
            if(i < argc - 1) {
                args.key = (char*) malloc(sizeof(char*)*strlen(argv[i+1]));
                strncpy(args.key, argv[i+1], strlen(argv[i+1]));
                i++;
            } else {
                printf("Usage error\n\n");
                help();
                exit(1);
            }
        }
        else {
            args.files[args.numfiles] = (char*) malloc(sizeof(char*) * strlen(argv[i]));
            strncpy(args.files[args.numfiles], argv[i], strlen(argv[i]));
            args.numfiles++;
        }
    }
    if(enc_dec_count != 1 || args.key == NULL) {
        printf("Usage error\n\n");
        help();
        exit(1);
    }
}

int main(int argc, char *argv[])
{

     
    parse_args(argc, argv);
    // printf("Password: %s\n", args.key);
    // printf("#Files: %d\n", args.numfiles);
    // printf("Mode: %d\n", args.mode);
    // for(int i = 0; i < args.numfiles; ++i)
    //     printf("File #%d: %s\n", i, args.files[i]);

	AES aes;
	int keylength = 256;
    CryptString key;
    key.set(args.key);
	AES::BlockMode block = AES::CBC;
    for(int i = 0; i < args.numfiles; ++i) {
        printf("%s\n", args.files[i]);
        string infile = args.files[i];
        if(args.mode == 0) {
            string outfile;
            if(strcmp(infile.c_str(), "-") == 0)
                outfile = infile;
            else
                outfile = infile + ".aes";
            printf("- Encrypting %s\n", infile.c_str());
            aes.EncryptFile(infile.c_str(), outfile.c_str(),key,keylength,block);
        }
        else if(args.mode == 1) {
            string outfile = infile;
            if(strcmp(infile.c_str(), "-") != 0)
                outfile[strlen(infile.c_str()) - 4] = '\0';
            printf("%s\n", outfile.c_str());
            printf("- Encrypting %s\n", infile.c_str());
            aes.DecryptFile(infile.c_str(), outfile.c_str(),key,keylength,block);
        }
    }


    exit(0);
	// cout << "\n\n##############################################\nWelcome to AES-File-Encryption and -Decryption\n##############################################\n\n\n";
	// switch(argc)
	// {
	// 	case 1: cout << "Please argument a file to de- and encrypt: AES <filename> [keylength BlockMode]\nor type AES --help to get info\n";
	// 			exit(0);
	// 	case 2: keylength = 256;
	// 			block = AES::CBC;
	// 			if(string(argv[1]) == string("--help"))
	// 			{
	// 				cout << "AES <filename> [keylength BlockMode]\n\n\tkeylength = 128 or 192 or (Standard) 256\n\tBlockMode = ECB (less safe) or (Standard) CBC (save)\n\n";
	// 				exit(0);
	// 			}
	// 			cout << "Start with Parameters: AES" << keylength << ", BlockMode: CBC\n\n";
	// 			break;
	// 	case 3: keylength = atoi(argv[2]);
	// 			block = AES::CBC;
	// 			if(keylength != 128 && keylength != 192 && keylength != 256)
	// 			{
	// 				cout << "Use valid AES-Keylength (128, 192 or 256 Bit)";
	// 				exit(0);
	// 			}
	// 			cout << "Start with Parameters: AES" << keylength << ", BlockMode: CBC\n\n";
	// 			break;
	// 	case 4: keylength = atoi(argv[2]);
	// 			if(keylength != 128 && keylength != 192 && keylength != 256)
	// 			{
	// 				cout << "Use valid AES-Keylength (128, 192 or 256 Bit)\n\n";
	// 				exit(0);
	// 			}
	// 			if(string(argv[3]) == string("CBC"))
	// 			{
	// 				block = AES::CBC;
	// 				cout << "Start with Parameters: AES" << keylength << ", BlockMode: CBC\n\n";
	// 			}
	// 			else if(string(argv[3]) == string("ECB"))
	// 			{
	// 				block = AES::ECB;
	// 				cout << "Start with Parameters: AES" << keylength << ", BlockMode: ECB\n\n";
	// 			}
	// 			else
	// 			{
	// 				block = AES::CBC;
	// 				cout << "Start with Parameters: AES" << keylength << ", BlockMode: CBC ("<< argv[3] << " not known)\n\n";
	// 			}
	// 			
	// 			break;
	// }
    //
	// // key.set("\x2b\x7e\x15\x16\x28\xae\xd2\xa6\xab\xf7\x15\x88\x09\xcf\x4f\x3c");
	// key.set("this is a test key");
	// aes25.DecryptFile(dcu.c_str(), outfile.c_str(), key, keylength, block);
	// unsigned char out[16];
	// unsigned char in[16] = "\x6b\xc1\xbe\xe2\x2e\x40\x9f\x96\xe9\x3d\x7e\x11\x73\x93\x17";
	// in[15] = '\x2a';
	// unsigned char kei[32] = "\x60\x3d\xeb\x10\x15\xca\x71\xbe\x2b\x73\xae\xf0\x85\x7d\x77\x81\x1f\x35\x2c\x07\x3b\x61\x08\xd7\x2d\x98\x10\xa3\x09\x14\xdf";
	// kei[31] = '\xf4';
	// aes25.EncryptBlock256(in, out, kei);
	// printf("\n\n");
	// for(int i = 0; i < 16; ++i)
	// {
	// 	printf("[%2x]", in[i]);
	// }
	// printf("\n\n");
	// for(int i = 0; i < 32; ++i)
	// {
	// 	printf("[%2x]", kei[i]);
	// }
	// printf("\n\n");
	// for(int i = 0; i < 16; ++i)
	// {
	// 	printf("[%2x]", out[i]);
	// }
	// aes25.DecryptBlock256(out, in, kei);
	// printf("\n\n");
	// for(int i = 0; i < 16; ++i)
	// {
	// 	printf("[%2x]", in[i]);
	// }
	// getchar();
	// exit(0);
	// return 0;
}


