#include "CryptString.h"
#include <iostream>
#include <string.h>
#include <stdlib.h>
CryptString::CryptString()
{
	this->blocks = 2;
	this->tempblocks = 1;
    this->str = (unsigned char *)malloc(tempblocks*16);
	resize();
}

CryptString::~CryptString()
{
	for(int i = 0; i < 32; i++)
		this->str[i] = 0x00;
}

void CryptString::set(const char *val)
{
	int length = strlen(val);
	for(int i = 0; i < 32; i++)
		this->str[i] = 0x00;
	for(int i = 0; i < length; i++)
		this->str[i] = (unsigned char) val[i];
}

void CryptString::resize()
{
	unsigned char *temp = (unsigned char *)malloc(tempblocks*16);
	for(int i = 0; i < tempblocks*16; i++)
		temp[i] = str[i];
	free(this->str);
	this->str = (unsigned char *) malloc(blocks*16);
	if(tempblocks < blocks)
	{
		for(int i = 0; i < blocks*16; i++)
			this->str[i] = 0x00;
		for(int i = 0; i < tempblocks*16; i++)
			this->str[i] = temp[i];
	}
	else
	{
		for(int i = 0; i < blocks*16; i++)
			this->str[i] = temp[i];
	}
	tempblocks = blocks;
	free(temp);
}

CryptString CryptString::operator+(CryptString crypt)
{
	if(crypt.blocks == 0)
	{
		return *this;
	}
	else if(this->blocks == 0)
	{
		return crypt;
	}
	else
	{
		CryptString crypt1;
		crypt1.blocks = this->blocks + crypt.blocks;
		crypt1.resize();
		for(int i = 0; i < this->blocks*16; i++)
			crypt1.str[i] = this->str[i];
		int leng = this->blocks*16;
		
		for(int i = leng; i < crypt1.blocks*16; i++)
			crypt1.str[i] = crypt.str[i-leng];
		return crypt1;
	}
}

CryptString CryptString::operator=(CryptString crypt)
{
	this->blocks = crypt.blocks;
	this->resize();
	for(int i = 0; i < this->blocks*16; i++)
		this->str[i]=crypt.str[i];
	return *this;
}

void CryptString::print()
{
	for(int i = 0; i < this->blocks*16; i++)
		std::cout << this->str[i];
	/*QString text;
	
	for(int i = 0; i < this->blocks*16; ++i)
	{
		text += "[" + QString::number((int)this->str[i], 16) + "]";
		if((i+1) % 16 == 0)
			text += "\n";
	}
	QLabel *neu = new QLabel(text);
	neu->show();*/
}
