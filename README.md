# File Crypter

Encrypt & Decrypt files using AES 256-bit CBC-mode

<a href="https://gitlab.com/swinkelhofer/filecrypter/-/jobs/90405743/artifacts/raw/filecrypter"><img src="https://gitlab.com/swinkelhofer/filecrypter/raw/master/img/download.png" width="32" height="32"> Download latest build</a>

## Build & Install

```bash
git clone https://gitlab.com/swinkelhofer/filecrypter.git
cd filecrypter
make
sudo make install
```

## Usage

```bash
filecrypter [-d|--decrypt] [-e|--encrypt] [-h|--help] [-p|--password] <file1>...<fileN>
```

### Encrypt files

```bash
filecrypter --encrypt --password "password" <file1> ... <fileN>

# Example
find . -name '*.png' | xargs -i filecrypter --encrypt --password "password" {}
```

### Decrypt files

```bash
filecrypter --decrypt --password "password" <file1.aes> ... <fileN.aes>

# Example
find . -name '*.aes' | xargs -i filecrypter --decrypt --password "password" {}
```
